# libft

A self-compiled library of useful C standard library functions as well as a few additional ones. One included useful function is ft_printf (works with basic format specifiers such as %s, %d, %h, %o, %i, %p).
(Makefile included to compile libft.a)